# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Inbox;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Inbox

=head1 DESCRIPTION

URL, linked to an L<actor|MightyNetwork::Doc::ActivityPub::Actor> where ActivityPub requests s·he’s a recipient of should be received.

=head1 HANDLING ACTIVITIES

The L<inbox|MightyNetwork::Doc::ActivityPub::Inbox> have to process ActivityPub L<activities|MightyNetwork::Doc::ActivityPub::Activity>.

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::ActivityPub::Actor>, L<MightyNetwork::Doc::ActivityPub::Activity>

=cut

1;
