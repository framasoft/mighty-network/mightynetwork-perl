# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub

=head1 DESCRIPTION

Documentation about ActivityPub. Mostly comes from L<Narf|https://framagit.org/narf>’s work available on L<http://frama.link/ActivityPubTech>.

=head2 REFERENCE

This is the class hierarchy of L<MightyNetwork::Doc::ActivityPub>.

=over 2

=item L<MightyNetwork::Doc::ActivityPub>

=over 2

=item L<MightyNetwork::Doc::ActivityPub::Activity>

=over 2

=item L<MightyNetwork::Doc::ActivityPub::Activity::Announce>

=item L<MightyNetwork::Doc::ActivityPub::Activity::Create>

=item L<MightyNetwork::Doc::ActivityPub::Activity::Delete>

=back

=item L<MightyNetwork::Doc::ActivityPub::Actor>

=item L<MightyNetwork::Doc::ActivityPub::Collection>

=item L<MightyNetwork::Doc::ActivityPub::Inbox>

=item L<MightyNetwork::Doc::ActivityPub::SharedInbox>

=item L<MightyNetwork::Doc::ActivityPub::Note>

=item L<MightyNetwork::Doc::ActivityPub::Outbox>

=back

=back

=head1 SEE ALSO

L<MightyNetwork::Doc>

=cut

1;
