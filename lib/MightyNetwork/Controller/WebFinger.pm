# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::WebFinger;
use Mojo::Base 'Mojolicious::Controller', -signatures;

=pod

=head1 NAME

MightyNetwork::Controller::WebFinger

=head1 DESCRIPTION

=head1 METHODS

L<MightyNetwork::Controller::WebFinger> inherits methods from L<Mojolicious::Controller> and adds the following methods.

=head2 user()

=cut

sub user($c) {
    my $resource = $c->param('resource');
    my @tmp      = split('@', $resource);
    my $username = $tmp[0];
    my $host     = $tmp[1];

    my $not_found = { 'error' => 'Actor not found' };

    if ($host -ne $c->req->headers->host) {
        $c->render(json => $not_found);
    }

    my $href = $c->url_for->('users', user => $username)->to_abs->to_string;

    $c->render(json => {
        subject => sprintf('acct:%s', $resource);
        aliases => [ $href ],
        links   => [{
            rel  => 'self',
            type => 'application/activity+json',
            href => $href
        }]
    });
}

=head1 SEE ALSO

L<MightyNetwork::Controller>, L<Mojolicious::Controller>, L<Mojo::Base>

=cut

1;
