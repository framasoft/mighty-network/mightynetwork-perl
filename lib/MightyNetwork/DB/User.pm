# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::User;
use Mojo::Base 'MightyNetwork::DB', -signatures;
use MightyNetwork::DB::ActivityPub::Follow;
use MightyNetwork::DB::ActivityPub::Actor;

has 'table' => 'users';
has 'id';
has 'username';
has 'password';
has 'created_at';

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::DB::User

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 DESCRIPTION

Model for users.

This structure is L<not federated|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 ATTRIBUTES

L<MightyNetwork::DB::User> inherits attributes from L<MightyNetwork::DB> and adds the following attributes.

=head2 id

Primary key, UUID.

    my $id = $user->id;
    $user->id('e69b4429-4b6b-4e3e-bb2d-411770cd27a1');

=head2 username

Username of the user. Will be used for sign-in and to create the L<actor|MightyNetwork::DB::ActivityPub::Actor> username.

    my $username = $user->username;
    $user->username('foo');

=head2 password

Salted hash of the user’s password.

    my $password = $user->password;
    $user->password('salted_hashed_password');

=head2 created_at

PostgreSQL’s timestamp. Date of creation of the user.

    my $created_at = $user->created_at;
    $user->created_at('2004-10-19 10:23:54');

=cut

=head1 METHODS

L<MightyNetwork::DB::Note> inherits methods from L<MightyNetwork::DB> and adds the following methods.

=head2 get_user_by_id($id)

Just a wrapper around L<MightyNetwork::DB#find_by_fields-hash-collection> method, with no C<collection> argument.

    my $user = MightyNetwork::DB::User->new(app => $c-> app)
                                      ->get_user_by_id($id);

=cut

sub get_user_by_id($c, $id) {
    return $c->find_by_fields({id => $id});
}

=head2 get_user_by_username($username)

Just a wrapper around L<MightyNetwork::DB#find_by_fields-hash-collection> method, with no C<collection> argument.

    my $user = MightyNetwork::DB::User->new(app => $c-> app)
                                      ->get_user_by_username($username);

=cut

sub get_user_by_username($c, $username) {
    return $c->find_by_fields({username => $username});
}

=head2 get_actor

Returns the L<MightyNetwork::DB::ActivityPub::Actor> object linked to this user.

    my $actor = $user->get_actor;

=cut

sub get_actor($c) {
    return MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                ->get_from_user($c);
}

=head2 get_followers

Returns a L<Mojo::Collection> of L<followers|MightyNetwork::DB::ActivityPub::Follow> linked to this user.

    my $followers_collection = $user->get_followers

=cut

sub get_followers($c) {
    return MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                 ->get_collection_from_actor($c->get_actor);
}

=head1 PRIVATE METHODS

L<MightyNetwork::DB::User> also adds the following private methods.

=head2 _is_username_taken($username)

Returns a boolean indicating if the username is already taken.

    if (MightyNetwork::DB::User->new(app => $c->app)->_is_username_taken('foo')) {
        …
    } else {
        …
    }

=cut

sub _is_username_taken($c, $username) {
    return $c->app->dbi->db->select(
        $c->table,
        'count(id) AS count',
        { username => $username }
    )->hashes->first->{count};
}

=pod

=head1 MODIFIED METHODS

L<MightyNetwork::DB::User> also modifies the following methods.

=head2 create($hash)

Before creating the database record, it checks if the username provided in the hash table is already taken.

Returns C<undef> if the username is already taken.

    my $created_object = MightyNetwork::DB::User->new(app => $c->app)->create({ foo => 'bar' });

See L<MightyNetwork::DB#create-hash> 

=cut

sub create($c, $h) {
    return undef if $c->_is_username_taken($h->{username});
    return $c->SUPER::create($h);
}

=head1 SEE ALSO

L<MightyNetwork::DB>, L<MightyNetwork::DB::ActivityPub::Actor>, L<MightyNetwork::DB::ActivityPub::Follow>, L<Mojo::Base>

=cut

1;
