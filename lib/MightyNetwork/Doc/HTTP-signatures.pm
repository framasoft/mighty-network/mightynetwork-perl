# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::HTTP-signatures;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::HTTP-signatures

=head1 DESCRIPTION

Documentation about HTTP-signatures.

The ActivityPub specification asked every application implementing the protocol to ensure users’ safety:


=over 2

=item

prevent identity theft

=item

prevent modifications of sent messages

=back

However, the specification does not suggest a solution for that.
The I<de facto> solution is HTTP requests signature.

=head1 DOCUMENTATION

Here’s the keys to understand and use HTTP signatures.

=head2 Asymetric cryptography

Every L<actor|MightyNetwork::Doc::Glossary#Actor> will have a public and a private key, provided by its L<instance|MightyNetwork::Doc::Glossary#Instance>.

The keys are created with the RSA algorithm, with a length of 4096 bits.

=head2 Digest header

You need to create the C<Digest> header before signing your HTTP response.

In order to do that, you need to transform the content of your response to base64, then calculate its SHA256 hashsum.

    use Mojo::Util qw(b64_encode);
    use Digest::SHA qw(sha256_base64);

    my $content = 'foobarbaz';
    my $base64  = b64_encode $content;
    my $digest  = 'SHA-256=' . sha256_base64($base64);

=head2 Signature creation

You will sign the concatenation of different headers: C<request-target> (like C<POST /foo>), C<Host>, C<Date>, C<Digest>.

See L<https://tools.ietf.org/id/draft-cavage-http-signatures-10.html#canonicalization> for details.

    use Mojo::Date;
    use Crypt::OpenSSL::RSA;
    use Mojo::Util qw(b64_encode);

    my $date                 = Mojo::Date->new(time)->to_string; # HTTP date format
    my $host                 = 'foo.org';
    my $concatenated_headers = <<EOF;
    (request-target): post /
    host: $host
    date: $date
    digest: $digest
    EOF

    my $rsa     = Crypt::OpenSSL::RSA->new_private_key($actor->private_key)
                                     ->use_sha256_hash();
    my $signing = b64_encode $rsa->sign($concatenated_headers);

    my $header  = sprintf('Signature keyId="%s",algorithm="rsa-sha256",headers="request-target host date digest",signature="%s"',
                          $actor->url.'#main-key', $signing);

Then use C<$date>, C<$digest> and C<$signing> in your request headers (respectively: C<Date>, C<Digest> and C<Authorization> headers).

=head2 Perl easier signature creation

Luckily, there is module to sign requests more easily: L<Authen::HTTP::Signature>.

    use Authen::HTTP::Signature;
    use HTTP::Request::Common;
    use Mojo::Date;
    my $date   = Mojo::Date->new(time)->to_string; # HTTP date format
    my $url    = 'https://foo.org';
    my $signer = Authen::HTTP::Signature->new(
        key    => $actor->private_key;
        key_id => $actor->url.'#main-key'
    );
    my $req = POST($url,
        Date    => $date,
        Digest  => $digest,
        Content => $body
    );

    my $signed_req = $signer->sign($req);

Then use C<$date>, C<$digest> and C<$signed_req> in your request headers (respectively: C<Date>, C<Digest> and C<Authorization> headers).

=head2 Digest verification

Before verifying the signature of the request, recreate its L<digest|MightyNetwork::Doc::HTTP-signatures#Digest-header> and verify it’s the same as the one sent in the C<Digest> header.

=head2 Signature verification

That’s quite easy: fetch the public key of the actor if you don’t already have it (it’s in the actor object that you got with L<WebFinger|MightyNetwork::Doc::WebFinger>).

Recreate the concatenation of the headers listed in the C<Authorization> header (the C<headers> part) and verify the signature with the public key.

    use Crypt::OpenSSL::RSA;
    use Mojo::Util qw(b64_decode);

    my $host   = $request->headers->host;
    my $date   = $request->headers->date;
    my $digest = $request->headers->digest;
    my $body   = $request->body;
    my $concatenated_headers = <<EOF;
    POST / HTTP/1.1
    Host: $host
    Date: $date
    Digest: $digest

    $body
    EOF

    my $signature = $request->headers->authorization;
    $signature    =~ s/.*signature="(.*?)".*/$1/;

    my $rsa = Crypt::OpenSSL::RSA->new_public_key($actor->public_key);
    if ($rsa->verify($concatenated_headers, b64_decode($signature))) {
        say "Request is valid!"
    } else {
        say "Request isn’t valid";
    }

=head2 Perl easier signature verification

Once again, L<Authen::HTTP::Signature> make it easier.

    use Authen::HTTP::Signature::Parser;
    use HTTP::Request::Common;

    my $req = POST($request->url,
        Date          => $request->headers->date,
        Digest        => $request->headers->digest,
        Authorization => $request->headers->authorization,
        Content       => $request->body
    );

    my $p;
    try {
        $p = Authen::HTTP::Signature::Parser->new($req)->parse();
    } catch {
        die "Parse failed: $_\n";
    };

    $p->key($actor->public_key);

    if ($p->verify()) {
        say "Request is valid!"
    } else {
        say "Request isn’t valid";
    };

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<Authen::HTTP::Signature>, L<HTTP::Request::Common>

=cut

1;
