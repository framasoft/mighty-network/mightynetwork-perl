# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::ActivityPub;
use Mojo::Base;

=pod

=head1 NAME

MightyNetwork::Controller::ActivityPub

=head1 DESCRIPTION

Empty module, just here to provide this doc.

=head1 REFERENCE

This is the class hierarchy of L<MightyNetwork::Controller::ActivityPub>.

=over 2

=item L<MightyNetwork::Controller::ActivityPub>

=over 2

=item L<MightyNetwork::Controller::ActivityPub::Follow>

=item L<MightyNetwork::Controller::ActivityPub::Inbox>

=item L<MightyNetwork::Controller::ActivityPub::Outbox>

=back

=back

=head1 SEE ALSO

L<MightyNetwork>, L<MightyNetwork::Controller>, L<Mojo::Base>

=cut


1;
