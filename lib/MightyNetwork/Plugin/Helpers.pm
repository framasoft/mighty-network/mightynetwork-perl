# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Plugin::Helpers;
use Mojo::Base 'Mojolicious::Plugin', -signatures, -async_await;
use Mojo::Pg;
use Mojo::URL;

=pod

=head1 NAME

MightyNetwork::Plugin::Helpers - Provides helpers for L<MightyNetwork>

=cut

sub register($self, $app, $conf) {
    $app->plugin('PgURLHelper');
    $app->helper(dbi                => \&_dbi);
    $app->helper(wf_url             => \&_wf_url);
    $app->helper(generate_uuid      => \&_generate_uuid);
    $app->helper(get_actor_url      => \&_get_actor_url);
    $app->helper(get_actor_from_url => \&_get_actor_from_url);

    my $sql        = $app->dbi;
    my $migrations = $sql->migrations;
    if ($app->mode eq 'development' && $ENV{MIGHTYNETWORK_DEV}) {
        $migrations->from_file('utilities/migrations/postgresql.sql')->migrate(0)->migrate(1);
    } else {
        $migrations->from_file('utilities/migrations/postgresql.sql')->migrate(1);
    }
}

=head1 METHODS

L<MightyNetwork::Plugin::Helpers> inherits all methods from L<Mojolicious::Plugin> and implements the following new ones.

=head2 dbi()

Returns a L<Mojo::Pg> object to allow database querying.

Uses L<Mojolicious::Plugin::PgURLHelper>.

    my $dbi = $c->dbi;

=cut

sub _dbi($c) {
    my $pgdb  = $c->config('pgdb');
    my $port  = (defined $pgdb->{port}) ? $pgdb->{port}: 5432;
    my $addr  = $c->pg_url({
        host => $pgdb->{host}, port => $port, database => $pgdb->{database}, user => $pgdb->{user}, pwd => $pgdb->{pwd}
    });

    state $pg = Mojo::Pg->new($addr);

    $pg->max_connections($pgdb->{max_connections}) if defined $pgdb->{max_connections};

    return $pg;
}

=head2 wf_url($host, $account)

Returns the string of the L<WebFinger|https://en.wikipedia.org/wiki/WebFinger> URL for searching C<$account> on C<$host>.

    my $wf_url = $c->wf_url('framapiaf.org', 'framasky@framapiaf.org');

=cut

sub _wf_url($c, $host, $account) {
    return Mojo::URL->new()
                    ->scheme('http')
                    ->host($host)
                    ->path('/.well-known/webfinger')
                    ->query(resource => sprintf('acct:%s', $account))
                    ->to_string;
}

=head2 generate_uuid()

Returns an L<UUID|https://en.wikipedia.org/wiki/Universally_unique_identifier> by querying the PostgreSQL database.

    my $uuid = $c->generate_uuid;

=cut

sub _generate_uuid($c) {
    return $c->app->dbi->db->select(undef, 'uuid_generate_v4()')
                           ->hashes->first->{uuid_generate_v4};
}

=head2 get_actor_url($url)

Fetches the URL of an ActivityPub actor (see L<MightyNetwork::DB::ActivityPub::Actor>) from a L<WebFinger|https://en.wikipedia.org/wiki/WebFinger> URL.

Returns an hash table:

    { actor_url => $actor_url, errors => \@errors }

C<$actor_url> is the ActivityPub URL of the actor.

C<\@errors> is an array reference containing encountered errors.

    my $wf_url = $c->wf_url('framapiaf.org', 'framasky@framapiaf.org');
    my $hash = $c->get_actor_url($wf_url);

=cut

async sub _get_actor_url($c, $url) {
    my $tx = await $c->ua->get_p($url => {'Accept' => 'application/json'});
    my ($actor_url, @errors);
    if ($tx->result->is_success) {
        my $wf_response = $tx->result->json;
        if ($wf_response) {
            if ($wf_response->{aliases}) {
                $actor_url = $wf_response->{aliases}->[0];
            } else {
                push @errors, $wf_response->{error};
            }
        } else {
            push @errors, 'Actor not found';
        }
    } elsif ($tx->result->code == 404) {
        push @errors, '404 - Actor not found';
    }
    return { actor_url => $actor_url, errors => \@errors };
}

=head2 get_actor_from_url($host, $username, $url)

Fetches ActivityPub actor (see L<MightyNetwork::DB::ActivityPub::Actor>) informations.

Uses L<get_actor_url|MightyNetwork::Plugin::Helpers#get_actor_url-url> under the hood.

Returns an hash table:

    { actor => $actor, errors => \@errors }

C<$actor> is a L<MightyNetwork::DB::ActivityPub::Actor> object.

C<\@errors> is an array reference containing encountered errors.

    my $wf_url = $c->wf_url('framapiaf.org', 'framasky@framapiaf.org');
    my $hash = $c->get_actor_from_url('framapiaf.org', 'framasky', $wf_url);

=cut

async sub _get_actor_from_url($c, $host, $username, $url) {
    my $actor;

    my $actor_url = await $c->get_actor_url($url);

    return { actor => undef, errors => $actor_url->{errors} } if (scalar(@{$actor_url->{errors}}));

    my $tx = await $c->ua->get_p($actor_url->{actor_url} => {'Accept' => 'application/activity+json'});
    my @errors;
    if ($tx->result->is_success) {
        my $actor_response = $tx->result->json;
        if ($actor_response) {
            $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                          ->get_by_url($actor_response->{id});
            unless ($actor) {
                my $hash = {
                    url        => $actor_response->{id},
                    username   => $actor_response->{preferredUsername},
                    host       => $host,
                    inbox      => $actor_response->{inbox},
                    outbox     => $actor_response->{outbox},
                    following  => $actor_response->{following},
                    followers  => $actor_response->{followers},
                    public_key => $actor_response->{publicKey}->{publicKeyPem}
                };
                $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                              ->create($hash);
            }
        } else {
            push @errors, 'Error actor';
        }
    } else {
        push @errors, sprintf('Error actor: status code %s', $tx->code);
    }
    return { actor => $actor, errors => \@errors };
}

=head1 SEE ALSO

L<MightyNetwork>, L<MightyNetwork::Plugin>, L<Mojolicious::Plugin>, L<Mojo::Pg>, L<Mojolicious::Plugin::PgURLHelper>, L<MightyNetwork::DB::ActivityPub::Actor>

=cut

1;
