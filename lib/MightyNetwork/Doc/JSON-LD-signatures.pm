# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::JSON-LD-signatures;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::JSON-LD-signatures

=head1 DESCRIPTION

L<JSON-LD|MightyNetwork::Doc::JSON-LD> messages have to be signed with the L<actor|MightyNetwork::Doc::ActivityPub::Actor>’s key.

Please note that every L<MightyNetwork::Doc::ActivityPub::Activity> are signed, just to be sure that the signature will be available in case we need it.
That is not something in L<MightyNetwork::Doc::ActivityPub> standard but it’s a I<de facto> standard.

=head1 IMPORTANT NOTE

Please note that this I<de facto> standard’s usage is decreasing: a lot of languages lacks libraries to handle JSON-LD signatures.
To ensure the origin of an activity, you can fetch the activity from its original server.

=head1 SPECIFICATION

See L<https://w3c-dvcg.github.io/ld-signatures/>.

=head1 PURPOSE

The signatures are used to authenticate the origin of an activity:

=over 2

=item

If actor C<A> send a L<note|MightyNetwork::Doc::ActivityPub::Note> and actor C<B> L<announce|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-announce> it (i.e. share it), actor C<C> get the boost.

C<C> verifies the signature of the boost, to be sure that the boost really comes from C<B>.

C<C> fetches the note to display it and verifies its signature, to be sure that the note really comes C<A>.

=item

Actor C<A> L<deletes|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-delete> a note, C<B> forwards that delete activity to C<C> who verifies the JSON-LD signature to be sure that C<A> really wants to delete the note.

=back

=head1 HOW TO CREATE/VERIFY JSON-LD SIGNATURES

Extracts from L<https://medium.com/@johnrcallahan/linked-data-signatures-with-ruby-3fa4dbc8e1fb> (L<archive.org link|https://web.archive.org/web/20190318080458/https://medium.com/@johnrcallahan/linked-data-signatures-with-ruby-3fa4dbc8e1fb>).

The process of signing a JSON-LD document includes:

=over 2

=item

resolving the context vocabularies (i.e., fetching them via their URLs in the @context]

=item

normalizing (or sometimes called ‘canonicalizing’) the document

=item

determining the signature value with a private key (using RSA or Ed25519)

=item

embedding the signature JSON with the metadata and signature value (not part of the JSON-LD document)

=back


Verifying a signed JSON-LD document includes:

=over 2


=item

extracting the signature block from the JSON-LD document (remove it as well)

=item

normalizing (or sometimes called ‘canonicalizing’) the remaining JSON-LD document

=item

verifying the signature value with the public key (using RSA or Ed25519))

=back

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::JSON-LD>, L<MightyNetwork::Doc::ActivityPub::Activity>, L<MightyNetwork::Doc::ActivityPub::Actor>, L<https://w3c-dvcg.github.io/ld-signatures/>

=cut

1;
