# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Actor;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Actor

=head1 DESCRIPTION

L<Federated|MightyNetwork::Doc::Glossary#Federated-non-federated-objects> object, linked or not to a L<user|MightyNetwork::Doc::Glossary#User>.

=head1 JSON-LD REPRESENTATION

This is what you get when requesting an L<actor|MightyNetwork::Doc::ActivityPub::Actor> from a L<WebFinger|MightyNetwork::Doc::WebFinger> search.

    {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Person",
        "id": "https://example.org/users/narf",
        "preferredUsername": "narf",
        "name": "narf",
        "following": "https://example.org/users/narf/following",
        "followers": "https://example.org/users/narf/followers",
        "inbox": "https://example.org/users/narf/inbox",
        "outbox": "https://example.org/users/narf/outbox",
        "endpoints": {
            "sharedInbox": "https://example.org/shared/inbox"
        },
        "publicKey": {
          "id": "https://example.org/users/narf#main-key",
          "owner": "https://example.org/users/narf",
          "publicKeyPem": "-----BEGIN PUBLIC KEY-----\n[…]\n-----END PUBLIC KEY-----\n"
        }
    }

As this object is federated, it needs a C<type> and an C<id>.

In ActivityPub specification, if the instance is C<https://example.org>, and the name of the L<actor|MightyNetwork::Doc::ActivityPub::Actor> is C<narf>, its C<id> can be:

=over 2

=item

C<https://example.org/users/narf>

=item

C<https://example.org/@narf>

=item

C<https://example.org/@/narf>

=item

etc

=back

The choice I<should> be coherent for all the L<actor objects|MightyNetwork::Doc::ActivityPub::Actor> of the instance.
Although, you can choose to give a random C<id> to each L<actor|MightyNetwork::Doc::ActivityPub::Actor>.

The L<actor|MightyNetwork::Doc::ActivityPub::Actor> I<needs> to present C<inbox> and C<outbox> keys which path depends on the C<id>.

It’s better if the C<followers> and C<following> keys are provided. Their path depends on the C<id> too.

Other keys are optionals.

=head1 REFRESHING REMOTE ACTORS’ INFORMATIONS

You I<should> refresh the informations you have in your database about remote actors from time to time (say every 48 hours).

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::WebFinger>

=cut

1;
