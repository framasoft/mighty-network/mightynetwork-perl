# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::JSON-LD;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::JSON-LD

=head1 DESCRIPTION

L<JSON-LD|MightyNetwork::Doc::JSON-LD> (JavaScript Object Notation for Linked Data), is a method of encoding Linked Data using JSON.

It’s JSON, but it can contain reference in addition or instead of the data.

This page will give you only an overview of JSON-LD.
Go to L<https://w3c.github.io/json-ld-syntax/#json-ld-grammar> to learn more.

=head2 @context

The C<@context> key gives the structure of the data, and their type.

    {
        "@context": {
            "id": {
                "@id": "https://my.standard.url/name",
                "@type": "string"
            },
            "name": {
                "@id": "https://my.standard.url/id",
                "@type": "@id"
            }
        },
        "user": {
            "id": 1,
            "name": "Foo"
        }
    }

The context can link to an URL that gives the structure.

    {
        "@context": {
            "user": "https://my.standard.url"
        },
        "user": {
            "id": 1,
            "name": "Foo"
        }
    }

You can avoid using C<@context> by using URL instead of keys.

    {
        "user": {
            "https://my.standard.url/id": 1,
            "https://my.standard.url/name": "Foo"
        }
    }

=head2 data

As seen L<before|MightyNetwork::Doc::JSON-LD#context>, data can be directly in the JSON-LD.

    {
        "@context": {
            "user": "https://my.standard.url"
        },
        "user": {
            "id": 1,
            "name": "Foo"
        }
    }

But it can also be fetched from an URL.

    {
        "@context": {
            "user": "https://my.standard.url"
        },
        "user": "https://my.user.url"
    }

Where C<https://my.user.url> will give

    {
        "id": 1,
        "name": "Foo"
    }

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::JSON-LD-signatures>, L<https://w3c.github.io/json-ld-syntax/>

=cut

1;
