CARTON=carton exec
MN=script/mighty_network

dev:
	$(CARTON) morbo $(MN) --listen http://0.0.0.0:3000 --watch lib/ --watch script/ --watch mighty_network.conf --watch templates/

devlog:
	multitail log/development.log

page:
	$(CARTON) -- script/mighty_network export /perldoc --to doc --base /mighty-network/mightynetwork-perl
	@cp doc/perldoc/index.html doc/
