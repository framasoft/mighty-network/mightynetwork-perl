# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::Notification;
use Mojo::Base 'Mojolicious::Controller', -signatures;

=pod

=head1 NAME

MightyNetwork::Controller::Notification

=head1 DESCRIPTION

=head1 METHODS

L<MightyNetwork::Controller::Notification> inherits methods from L<Mojolicious::Controller> and adds the following methods.

=head2 notifications()

=cut

sub notifications($c) {
}

=head1 SEE ALSO

L<MightyNetwork::Controller>, L<Mojolicious::Controller>, L<Mojo::Base>

=cut

1;
