# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller;
use Mojo::Base;

=pod

=head1 NAME

MightyNetwork::Controller

=head1 DESCRIPTION

Empty module, just here to provide this doc.

The L<MightyNetwork::Controller> modules handle HTTP requests sent to L<MightyNetwork>.

=head1 REFERENCE

This is the class hierarchy of L<MightyNetwork::Controller>.

=over 2

=item L<MightyNetwork::Controller>

=over 2

=item L<MightyNetwork::Controller::ActivityPub>

=over 2

=item L<MightyNetwork::Controller::ActivityPub::Follow>

=item L<MightyNetwork::Controller::ActivityPub::Inbox>

=item L<MightyNetwork::Controller::ActivityPub::Outbox>

=back

=item L<MightyNetwork::Controller::Auth>

=item L<MightyNetwork::Controller::Index>

=item L<MightyNetwork::Controller::Misc>

=item L<MightyNetwork::Controller::Note>

=item L<MightyNetwork::Controller::Notification>

=item L<MightyNetwork::Controller::Users>

=item L<MightyNetwork::Controller::WebFinger>

=back

=back

=head1 SEE ALSO

L<MightyNetwork>, L<Mojo::Base>

=cut


1;
