# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::Users;
use Mojo::Base 'Mojolicious::Controller', -signatures, -async_await;
use Mojo::Collection;
use Mojo::URL;
use Mojo::UserAgent;
use MightyNetwork::DB::ActivityPub::Actor;
use MightyNetwork::DB::Note;

=pod

=head1 NAME

MightyNetwork::Controller::WebFinger

=head1 DESCRIPTION

=head1 METHODS

L<MightyNetwork::Controller::WebFinger> inherits methods from L<Mojolicious::Controller> and adds the following methods.

=head2 get_user()

=cut

sub get_user($c) {
}

=head2 get_users()

=cut

sub get_users($c) {
    $c->render(template => 'search');
}

=head2 search_users()

=cut

async search_users => sub ($c) {
    my $search = $c->param('user_searched');

    return $c->render(
        template => 'search',
        errors   => ['Your search is empty.']
    ) unless $search;

    my ($username, $host) = split('@', $search);

    my @errors;
    push @errors, 'You have to specify username.'     unless $username;
    push @errors, 'You have to specify user\'s host.' unless $host;

    if (scalar(@errors)) {
        return $c->render(
            template => 'search',
            errors   => \@errors
        );
    }

    if ($host eq $c->req->url->to_abs->host) {
        my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                         ->get_local_by_username($username);
        return $c->redirect_to($c->url_for('account', id => $actor->id));
    } else {
        my $actor_from_url = await $c->get_actor_from_url($host, $username, $c->wf_url($host, $search));

        push @errors, @{$actor_from_url->{errors}};
        if (scalar(@errors)) {
            return $c->render(
                template => 'search',
                errors   => \@errors
            );
        }

        my $actor = $actor_from_url->{actor};
        $actor->app(undef);
        return $c->redirect_to($c->url_for('account', id => $actor->id));
    }
};

=head2 account()

=cut

sub account ($c) {
    my $id = $c->param('id');

    my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                     ->find_by_('id', $id);

    my $notes = Mojo::Collection->new();
    if ($actor) {
        $notes = MightyNetwork::DB::Note->new(app => $c->app)
                                        ->find_actor_notes($actor);
    }

    $c->render(
        template     => 'user',
        actor        => $actor,
        notes        => $notes,
        is_local     => (defined($actor) && $actor->host eq $c->req->url->to_abs->host),
        follow_state => undef,
        messages     => ($actor) ? undef : 'Unable to find the actor'
    );
};

=head2 get_actors()

=cut

sub get_actors($c) {
}

=head2 username()

=cut

sub username($c) {
}

=head2 get_note()

=cut

sub get_note($c) {
}

=head2 outbox()

=cut

sub outbox($c) {
}

=head2 inbox()

=cut

sub inbox($c) {
}

=head2 webfinger()

=cut

sub webfinger($c) {
}

=head1 SEE ALSO

L<MightyNetwork::Controller>, L<Mojolicious::Controller>, L<Mojo::Base>

=cut

1;
