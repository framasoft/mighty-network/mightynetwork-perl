# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DefaultConfig;
require Exporter;
@ISA = qw(Exporter);
@EXPORT_OK = qw($default_config);
our $default_config = {
    instance => 'MightyNetwork',
};

=pod

=head1 NAME

MightyNetwork::DefaultConfig

=head1 SYNOPSIS

    use MightyNetwork::DefaultConfig qw($default_config);
    my $config = $self->plugin('Config' => {
        default => $default_config
    });

=head1 DESCRIPTION

Export a C<$default_config> variable, used to provide default values for L<MightyNetwork> configuration file.

The aim of using a module for that is the reusability of the default values in different parts of the software (in commands for example).

=head1 SEE ALSO

L<MightyNetwork>, L<Mojolicious::Plugin::Config>

=cut

1;
