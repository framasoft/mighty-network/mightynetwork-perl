# Installation

## Dependencies

Let's install some packages:

```
sudo apt-get install build-essential libssl-dev libpq-dev zlib1g-dev cpanminus git
```

```
cpanm Carton inc::Module::Install
```

## Installation

```
git clone https://framagit.org/framasoft/mighty-network/mightynetwork-perl.git
cd mightynetwork-perl
carton install
cp mighty_network.conf.template mighty_network.conf
vi mighty_network.conf
```

The mighty_network.conf.template is self-documented. Please report an [issue](https://framagit.org/framasoft/mighty-network/mightynetwork-perl/issues) if something is not clear.

## Create the databases

If you want to install PostgreSQL on your server:
```
sudo apt-get install postgresql-server postgresql-contrib
```

On the server that hosts PostgreSQL:

```
su - postgres
createuser mighty_network_user -P
createdb -O mighty_network_user mighty_network_db
createuser mighty_network_minion_user -P
createdb -O mighty_network_minion_user mighty_network_minion_db
psql mighty_network_db
CREATE EXTENSION "uuid-ossp";
\q
exit
```
