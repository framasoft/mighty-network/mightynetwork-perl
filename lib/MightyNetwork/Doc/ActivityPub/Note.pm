# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Note;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Note

=head1 DESCRIPTION

L<Federated|MightyNetwork::Doc::Glossary#Federated-non-federated-objects> object of C<Note> type.

Inherits all properties from the L<Object|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object> type (doesn’t add new ones at the time of writing).

See L<https://www.w3.org/TR/activitystreams-vocabulary/#dfn-note> for details.

=head1 JSON-LD REPRESENTATION

    {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Note",
        "id" : "https://example.org/users/narf/note/5b44c2974a67a67eec472d82",
        "to" : [ "https://www.w3.org/ns/activitystreams#Public" ],
        "cc" : [ "https://example.org/users/narf/followers" ],
        "attributedTo":"https://example.org/users/narf",
        "content": "Hello world !",
        "published":"2018-07-10T14:28:39.889Z",
    }

As this object is federated, it needs a C<type> (C<Note>, obviously) and an C<id>.

=head1 PROPERTIES

=head2 to

Refers to the recipients of the note.

=over 2

=item

C<"https://www.w3.org/ns/activitystreams#Public"> means that the message is public, i.e. viewable by all the L<fediverse|MightyNetwork::Doc::Glossary#Fediverse>.

=item

C<"https://example.org/users/narf/followers"> means that the message is for L<followers|MightyNetwork::Doc::Glossary#Followers> of Narf (an L<actor|MightyNetwork::Doc::ActivityPub::Actor>) only.

=item

C<"https://example.org/users/framasky"> means that the message is for this L<actor|MightyNetwork::Doc::ActivityPub::Actor> only (as C<to> is an array, you can put more than one actor).

=back

=head2 cc

Refers to the secondary recipients of the note.

See L<to|#to>

=head2 attributedTo

The C<id> of the L<actor|MightyNetwork::Doc::ActivityPub::Actor> that emitted the message.

=head2 content

Well… the content of the note ¯\_(ツ)_/¯

=head2 published

Timestamp, date and time of message’s emission.

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::Glossary>

=cut

1;
