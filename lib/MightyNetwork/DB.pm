# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB;
use Mojo::Base -base, -signatures;
use Mojo::JSON qw(encode_json);
use Data::Structure::Util qw(unbless);

has 'app';
has 'record' => 0;
has 'table';

=pod

=head1 NAME

MightyNetwork::DB - DB abstraction layer

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 DESCRIPTION

Provides common methods to objects models.

Uses L<Mojo::Pg> for database querying.

=head1 ATTRIBUTES

L<MightyNetwork::DB> implements the following attributes.

=head2 app

A Mojolicious instance, needed to access helpers (like the database accessor)

    my $app = $object->app;
    $object->app(Mojolicious->new());

=head2 record

Boolean, indicates if the object has been registered in database

    my $record = $object->record;
    $object->record(1);

=head2 table

The name of the DB table used by the object

    my $table = $object->table;
    $object->table('foo');

=head1 METHODS

L<MightyNetwork::DB> inherits all methods from L<Mojo::Base> and implements the following new ones.

=head2 _as_struct()

Private method.

Transform an object in a hash table.

Attributes C<app>, C<record> and C<table> are removed from the hash table.

    my $hash = $object->_as_struct();

=cut

sub _as_struct($c) {
    $c = unbless($c);

    delete $c->{app};
    delete $c->{record};
    delete $c->{table};

    return $c;
}

=head2 to_json(@to_delete)

Transform an object in a JSON hash table.

Attributes listed in C<@to_delete> argument will be removed from the JSON hash table.

    my $json = $object->to_json('foo', 'bar');

=cut

sub to_json($c, @to_delete) {
    $c = $c->_as_struct();
    for my $field (@to_delete) {
        delete $c->{$field};
    }

    return encode_json($c);
}

=head2 create($hash)

Insert C<$hash> values into the object table and return the object with the values of the created record.

The returned object has C<record> attribute to 1.

    my $created_object = $object->create({ foo => 'bar' });

=cut

sub create($c, $h) {
    my $r = $c->app->dbi->db->insert($c->table, $h, undef, { returning => '*' });
    return $c->map_fields_to_attr($r->hashes->first)->record(1);
}

=head2 write()

Write the data of the object in database. Create the record if not existing, update it otherwise.

Returns the object with C<record> attribute set to 1.

    $object->write();

=cut

sub write($c) {
    if ($c->record) {
        return $c->update($c->_as_struct);
    } else {
        return $c->create($c->_as_struct);
    }
}

=head2 update($hash)

Update an object attributes and update its database record.

Returns the updated object.

    $object->update($hash);

=cut

sub update($c, $h) {
    my $r = $c->app->dbi->db->update($c->table, $h, $c->_as_struct, { returning => '*' });
    return $c->map_fields_to_attr($r->hashes->first);
}

=head2 delete()

Delete the object from database.

    $object->delete();

=cut

sub delete($c) {
    return $c->app->dbi->db->delete($c->table => $c->_as_struct);
}

=head2 find_by($field, $value, $collection)

Wrapper of L<find_by_fields|MightyNetwork::DB#find_by_fields> method.

Find records whose C<$field> attribute is equal to C<$value>.

Returns an object with C<record> attribute set to 1 if C<$collection> is not true and if there is only one record matching.

Returns a collection of matching objects if C<$collection> is true.

Returns C<undef> if no record is found.

    my $object     = $c->find_by('foo', 'bar');
    my $collection = $c->find_by('foo', 'bar', 1);

=cut

sub find_by_($c, $i, $j, $collection) {
    return $c->find_by_fields_({$i => $j}, $collection);
}

=head2 find_by_fields($hash, $collection)

Find records whose attributes matches the hash table argument.

Returns an object with C<record> attribute set to 1 if C<$collection> is not true and if there is only one record matching.

Returns a collection of matching objects if C<$collection> is true.

Returns C<undef> if no record is found.

    my $object     = $c->find_by({'foo' => 'bar'});
    my $collection = $c->find_by({'foo' => 'bar'}, 1);

=cut

sub find_by_fields($c, $h, $collection) {
    my $r = $c->app->dbi->db->select($c->table, undef, $h);

    if ($r->rows == 1) {
        if ($collection) {
            return c($c->map_fields_to_attr($r->hashes->first)->record(1));
        } else {
            return $c->map_fields_to_attr($r->hashes->first)->record(1);
        }
    } elsif ($collection && $r->rows > 1) {
        return $r->hashes->map(sub {
            return $c->new($_)->record(1);
        });
    }

    return undef;
}

=head2 map_fields_to_attr($hash)

Map each entry of given hash table to object attributes.

Returns an object.

    my $object = $c->map_fields_to_attr({'foo' => 'bar'});

=cut

# Map fields as attributes
sub map_fields_to_attr($c, $h) {
    while (my ($k, $v) = each %{$h}) {
        # Add each field as object's attribute
        $c->{$k} = $v;
    }

    return $c;
}

=head1 REFERENCE

This is the class hierarchy of L<MightyNetwork::DB>.

=over 2

=item L<MightyNetwork::DB>

=over 2

=item L<MightyNetwork::DB::ActivityPub>

=over 2

=item L<MightyNetwork::DB::ActivityPub::Activity>

=item L<MightyNetwork::DB::ActivityPub::Actor>

=item L<MightyNetwork::DB::ActivityPub::Collection>

=item L<MightyNetwork::DB::ActivityPub::Follow>

=back

=item L<MightyNetwork::DB::Note>

=item L<MightyNetwork::DB::Notification>

=item L<MightyNetwork::DB::User>

=back

=back

=head1 SEE ALSO

L<MightyNetwork>, L<Mojolicious::Base>, L<Mojo::Pg>

=cut

1;
