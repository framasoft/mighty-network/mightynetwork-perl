# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::ActivityPub;
use Mojo::Base;

=pod

=head1 NAME

MightyNetwork::DB::ActivityPub

=head1 DESCRIPTION

Empty module, just here to provide this doc.

=head1 REFERENCE

This is the class hierarchy of L<MightyNetwork::DB::ActivityPub>.

=over 2

=item L<MightyNetwork::DB::ActivityPub>

=over 2

=item L<MightyNetwork::DB::ActivityPub::Activity>

=item L<MightyNetwork::DB::ActivityPub::Actor>

=item L<MightyNetwork::DB::ActivityPub::Collection>

=item L<MightyNetwork::DB::ActivityPub::Follow>

=back

=back

=head1 SEE ALSO

L<MightyNetwork>, L<MightyNetwork::DB>, L<Mojolicious::Base>

=cut


1;
