# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Collection;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Collection

=head1 DESCRIPTION

L<Federated|MightyNetwork::Doc::Glossary#Federated-non-federated-objects> object of C<Collection> type.

It’s a list of other L<objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>: L<actors|MightyNetwork::Doc::ActivityPub::Actor>, L<notes|MightyNetwork::Doc::ActivityPub::Note>…
Collections can be L<paged|https://www.w3.org/TR/activitystreams-core/#h-paging> and so may not contain items but indicates where to find the L<CollectionPage|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-collectionpage> objects that contains the items.

Inherits all properties from the L<Object|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object> type.

See L<https://www.w3.org/TR/activitystreams-core/#collection> for details.

=head1 JSON-LD REPRESENTATION

    {
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1"
        ],
        "id": "https://thisinstance.org/users/narf/followers",
        "type": "Collection",
        "totalItems": 3,
        "items": [
            "https://anotherinstance.org/users/ren",
            "https://yetanotherinstance.social/users/pierre",
            "https://thisinstance.org/users/nathalie"
        ]
    }

As this object is federated, it needs a C<type> (C<Collection>, obviously) and an C<id>.

=head1 ORDEREDCOLLECTION

An C<OrderedCollection> is a subtype of C<Collection> in which members of the logical collection are assumed to always be strictly ordered.
It inherits all properties from C<Collection> but use C<orderedItems> instead of C<items>.

=head1 PROPERTIES

=head2 totalItems

Total number of items in the C<Collection>.

=head2 items / orderedItems

Items in the C<Collection>.

=head2 first

In a paged C<Collection>, indicates the furthest preceeding page of items in the collection.

=head2 last

In a paged C<Collection>, indicates the furthest proceeding page of the collection.

=head2 current

In a paged C<Collection>, indicates the page that contains the most recently updated member items.

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::Glossary>

=cut

1;
