# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::SharedInbox;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::SharedInbox

=head1 DESCRIPTION

URL which mutualizes, at instance’s level, L<inbox|MightyNetwork::Doc::ActivityPub::Inbox> to receive public L<activities|MightyNetwork::Doc::ActivityPub::Activity> and dispatch them internally (to avoid getting thousand of activities for each L<actor|MightyNetwork::Doc::ActivityPub::Actor> who shall receive the activity).

=head1 HANDLING ACTIVITIES

The L<sharedInbox|MightyNetwork::Doc::ActivityPub::SharedInbox> have to process ActivityPub L<activities|MightyNetwork::Doc::ActivityPub::Activity> and dispatch them internally.

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::ActivityPub::Actor>, L<MightyNetwork::Doc::ActivityPub::Activity>

=cut

1;
