# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
requires 'inc::Module::Install';
requires 'Scalar::Util', '>= 1.50';
requires 'Mojolicious';
requires 'Mojo::Pg';
requires 'Mojolicious::Command::export';
requires 'Mojolicious::Plugin::Authentication';
requires 'Mojolicious::Plugin::DebugDumperHelper';
requires 'Mojolicious::Plugin::PgURLHelper';
requires 'Mojolicious::Plugin::PODViewer';
requires 'Mojolicious::Plugin::Status';
requires 'Crypt::OpenSSL::Random';
requires 'Crypt::OpenSSL::RSA';
requires 'Crypt::PBKDF2';
requires 'Data::Structure::Util';
requires 'DateTime';
requires 'Authen::HTTP::Signature';
requires 'Future::AsyncAwait';

# Mojolicious optional deps
feature 'optional_deps' => sub {
    requires 'Cpanel::JSON::XS';
    requires 'EV';
    requires 'IO::Socket::Socks';
    requires 'Net::DNS::Native';
    requires 'Role::Tiny';
};
