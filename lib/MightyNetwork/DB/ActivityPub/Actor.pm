# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::ActivityPub::Actor;
use Mojo::Base 'MightyNetwork::DB', -signatures;
use Crypt::OpenSSL::RSA;
use Crypt::OpenSSL::Random;

has 'table' => 'ap_actors';
has 'id';
has 'user_id';
has 'url';
has 'username';
has 'host';
has 'inbox';
has 'outbox';
has 'following';
has 'followers';
has 'public_key';
has 'private_key';
has 'created_at';

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::DB::ActivityPub::Actor

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 DESCRIPTION

Model for ActivityPub actors

This object is L<federatedMightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 ATTRIBUTES

L<MightyNetwork::DB::ActivityPub::Actor> inherits attributes from L<MightyNetwork::DB> and adds the following attributes.

=head2 id

Primary key, UUID.

    my $id = $object->id;
    $object->id('30790b80-c7cc-480a-a502-4ca456f09a38');

=head2 user_id

Id (UUID) of the L<user|MightyNetwork::DB::User> that actor is linked to. Optional.

    my $user_id = $object->user_id;
    $object->user_id('e69b4429-4b6b-4e3e-bb2d-411770cd27a1');

=head2 url



    my $url = $object->url;
    $object->url(3);

=head2 username

    my $username = $object->username;
    $object->username(3);

=head2 host

    my $host = $object->host;
    $object->host(3);

=head2 inbox

    my $inbox = $object->inbox;
    $object->inbox(3);

=head2 outbox

    my $outbox = $object->outbox;
    $object->outbox(3);

=head2 following

    my $following = $object->following;
    $object->following(3);

=head2 followers

    my $followers = $object->followers;
    $object->followers(3);

=head2 public_key

    my $public_key = $object->public_key;
    $object->public_key(3);

=head2 private_key

    my $private_key = $object->private_key;
    $object->private_key(3);

=head2 created_at

    my $created_at = $object->created_at;
    $object->created_at(3);

=head1 METHODS

L<MightyNetwork::DB::ActivityPub::Actor> inherits methods from L<MightyNetwork::DB> and adds the following methods.

=head2 create_from_user($user, $url)

Given a L<MightyNetwork::DB::URL> and an URL, creates a linked L<MightyNetwork::DB::ActivityPub::Actor>.
See L<MightyNetwork::DB::ActivityPub::Actor#generate_actor_from_user-user-url> for details.

    my $user  = MightyNetwork::DB::User->new(…);
    my $url   = 'https://example.org';
    my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                     ->create_from_user($user, $url);

=cut

sub create_from_user($c, $user, $url) {
    return $c->create($c->_generate_actor_from_user($user, $url));
}

=head2 get_from_user($user)

Retrieve the L<MightyNetwork::DB::ActivityPub::Actor> linked to the given L<MightyNetwork::DB::User>.

    my $user  = MightyNetwork::DB::User->new(…);
    my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                     ->get_from_user($user);

=cut

sub get_from_user($c, $user) {
    return $c->find_by_fields(user_id => $user->id);
}

=head2 get_local_by_username($username)

Retrieve the local L<MightyNetwork::DB::ActivityPub::Actor> linked to the given C<$username>.

    my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                     ->get_local_by_username('foo');

=cut

sub get_local_by_username($c, $username) {
    return $c->find_by_fields_({username => $username, user_id => undef});
}

=head2 get_by_url($url)

Retrieve the L<MightyNetwork::DB::ActivityPub::Actor> linked to the given C<$url>.

    my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                     ->get_by_url('https://example.org/@foo');

=cut

sub get_by_url($c, $url) {
    return $c->find_by_('url', $url);
}

=head2 get_followers()

Return a L<Mojo::Collection> of L<MightyNetwork::DB::ActivityPub::Follow> linked to the L<MightyNetwork::DB::ActivityPub::Actor>

    my $collection = $actor->get_followers;

=cut

sub get_followers($c) {
    return MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                 ->get_collection_from_actor($c);
}

sub create_local_actor($c, $h) {
}

sub create_remote_actor($c, $h) {
}

=head1 PRIVATE METHODS

L<MightyNetwork::DB::ActivityPub::Actor> also adds the following private methods.

=head2 _generate_actor_from_user($user, $url)

Return a hash reference containing all needed attributes to create a L<MightyNetwork::DB::ActivityPub::Actor> object.

L<MightyNetwork::DB::ActivityPub::Actor> created with this hash will be linked to the L<MightyNetwork::DB::User> passed as argument, and its differents URLs will be created from the C<$url> argument.

4096 bits RSA private and public keys will be created in the process and included in the resulting hash (uses L<Crypt::OpenSSL::RSA>).

    my $user       = MightyNetwork::DB::User->new(…);
    my $url        = 'https://example.org';
    my $actor      = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
    my $actor_hash = $actor->_generate_actor_from_user($user, $url);
    my $actor      = $actor->create($actor_hash);

=cut

sub _generate_actor_from_user($c, $user, $url) {
    my $host = $url->to_abs->host;
    $url     = $url->path('users/'.$user->username)->to_abs->to_string;

    do {
        Crypt::OpenSSL::RSA->import_random_seed();
    } while (!Crypt::OpenSSL::Random::random_status());

    my $rsa = Crypt::OpenSSL::RSA->generate_key(4096);
    return {
        user_id     => $user->id,
        username    => $user->username,
        host        => $host,
        url         => $url,
        inbox       => $url.'/inbox',
        outbox      => $url.'/outbox',
        following   => $url.'/following',
        followers   => $url.'/followers',
        public_key  => $rsa->get_public_key_x509_string,
        private_key => $rsa->get_private_key_string,
        created_at  => $user->created_at
    }
}

=head1 MODIFIED METHODS

L<MightyNetwork::DB::ActivityPub::Actor> also modifies the following methods.

=head2 to_json(@to_delete)

It adds the C<user_id> and C<private_key> attributes to the list of attributes to delete from the JSON representation of the object.

    my $json = $object->to_json('foo', 'bar');

See L<MightyNetwork::DB#to_json-to_delete> for the original method.

=cut

sub to_json($c, @to_delete) {
    push @to_delete, 'user_id', 'private_key';
    return $c->SUPER::to_json(@to_delete);
}

=head1 SEE ALSO

L<MightyNetwork::DB>, L<MightyNetwork::DB::User>, L<MightyNetwork::DB::ActivityPub::Follow>, L<Mojo::Base>, L<Crypt::OpenSSL::RSA>, L<Mojo::Collection>

=cut

1;
