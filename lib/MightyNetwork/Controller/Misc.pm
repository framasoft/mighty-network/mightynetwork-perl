# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::Misc;
use Mojo::Base 'Mojolicious::Controller', -signatures;

=pod

=head1 NAME

MightyNetwork::Controller::Misc

=head1 DESCRIPTION

=head1 METHODS

L<MightyNetwork::Controller::Misc> inherits methods from L<Mojolicious::Controller> and adds the following methods.

=head2 index()

Renders the C<index> template (containing a form to post a note) if the user is logged in.

Renders the C<welcome> template otherwise.

=cut

sub index($c) {
    if ($c->is_user_authenticated) {
        $c->render(
            template => 'index',
        )
    } else {
        $c->render(
            template => 'welcome',
            instance => $c->config('instance')
        )
    }
}

=head1 SEE ALSO

L<MightyNetwork::Controller>, L<Mojolicious::Controller>, L<Mojo::Base>

=cut

1;
