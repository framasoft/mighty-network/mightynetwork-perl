# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::Glossary;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::Glossary

=head1 DESCRIPTION

ActivityPub glossary.

=head1 DEFINITIONS

Those terms are required to understand implementation of ActivityPub protocol.

=head2 Activity

It’s the L<federated object|MightyNetwork::Doc::Glossary#Federated-non-federated-objects> representing an action made by an L<actor|MightyNetwork::Doc::Glossary#actor> in the L<fediverse|MightyNetwork::Doc::Glossary#Fediverse>.

Caracterised by the person who do the action (the L<actor|MightyNetwork::Doc::Glossary#actor>) and the action’s object.

=head2 Activity Streams

It’s a format specification used by the ActivityPub protocol. Used in L<JSON-LD|MightyNetwork::Doc::JSON-LD> messages.

See L<https://www.w3.org/TR/activitystreams-core/> for details.

=head2 Actor

It’s a L<federated object|MightyNetwork::Doc::Glossary#Federated-non-federated-objects> which can represent multiple things in the L<fediverse|MightyNetwork::Doc::Glossary#Fediverse>:

=over 2

=item

a L<user|MightyNetwork::Doc::Glossary#User> (an ActivityPub L<I<Person>|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-person>)

=item

an L<instance|MightyNetwork::Doc::Glossary#Federated-non-federated-instances> (an ActivityPub L<I<Application>|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-application>)

=item

a channel (an ActivityPub L<I<group>|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-group>)

=item

see L<https://www.w3.org/TR/activitystreams-vocabulary/#actor-types> for more.

=back

While talking about actor, it means fediverse’s actor.

=head2 Federated / non-federated (instances)

L<Instances|MightyNetwork::Doc::Glossary#Instance> are I<federated> when they can exchange informations: requests from one instance are understood by the others.

=head2 Federated / non-federated (objects)

An object is I<federated> if a copy of that object is sent or made available (for ex. not copied but available by searching) to the L<fediverse|MightyNetwork::Doc::Glossary#Fediverse>.

An object is I<non federated> if it stays in the database of the L<instances|MightyNetwork::Doc::Glossary#Instance> of origin and is not — or should not be — transfered to another instanc.
For disambiguation, we’ll talk, in this documentation, of I<structures> for designing I<non federated> objects.

=head2 Fediverse

Contraction of I<federation> and I<universe> which refers to all L<instances|MightyNetwork::Doc::Glossary#Instance> where applications implementing the ActivityPub protocol are installed.

=head2 Followers

Refers to the URL, belonging to an L<actor|MightyNetwork::Doc::Glossary#actor>, where are listed the actors who want to receive the informations shared by her/him (those actors I<follows> her/him).

=head2 Following

Refers to the URL, belonging to an L<actor|MightyNetwork::Doc::Glossary#actor>, where are listed the actors s·he follows and wants to receive their shared informations.

=head2 Inbox

Refers to the URL where an L<actor|MightyNetwork::Doc::Glossary#actor> receive the L<activities|MightyNetwork::Doc::Glossary#Activity> s·he’s an recipient of.

=head2 Instance

Server providing a service implementing ActivityPub.

See L<Mastodon|https://joinmastodon.org>, L<PeerTube|https://joinpeertube.org>, L<Nextcloud|https://nextcloud.com/blog/activitypub-the-new-standard-for-decentralized-networks/>…

=head2 JSON-LD

See L<MightyNetwork::Doc::JSON-LD>.

=head2 Outbox

Refers to the URL, belonging to an L<actor|MightyNetwork::Doc::Glossary#Actor>, where are listed the L<activities|MightyNetwork::Doc::Glossary#Activity> s·he’s the sender of.

Should return a L<collection|https://www.w3.org/TR/activitystreams-vocabulary/#dfn-collection> of L<activities|MightyNetwork::Doc::Glossary#Activity>.

Useful to fetch past activities when you follow a new actor.

=head2 User

I<Non-federated> structure representing a registered user on the L<Instance|MightyNetwork::Doc::Glossary#Instance>, containing security informations (credentials…) allowing the user to log in on the instance.

=head2 Shared inbox

For an L<instance|MightyNetwork::Doc::Glossary#Instance>, it’s an URL grouping the L<inbox|MightyNetwork::Doc::Glossary#Inbox> of its L<actors|MightyNetwork::Doc::Glossary#Actor>, used to receive the public L<activities|MightyNetwork::Doc::Glossary#Activity> and redispatch them internally to the concerned actors.

If you have ten actors following someone on a different instance, it’s better to get only one request for its public activity instead of ten.
That’s the purpose of the shared inbox.

=head2 WebFinger

See L<MightyNetwork::Doc::WebFinger>.

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::JSON-LD>, L<MightyNetwork::Doc::WebFinger>

=cut

1;
