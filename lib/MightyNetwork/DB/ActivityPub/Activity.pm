# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::ActivityPub::Activity;
use Mojo::Base 'MightyNetwork::DB', -signatures;

has 'table' => 'ap_activities';
has 'context';
has 'xtype';
has 'actor';
has 'object';
has 'signature';
has 'published';

=pod

=head1 NAME

MightyNetwork::DB::ActivityPub::Activity

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 DESCRIPTION

Model for ActivityPub activities.

=head1 ATTRIBUTES

L<MightyNetwork::DB::ActivityPub::Activity> inherits attributes from L<MightyNetwork::DB> and adds the following attributes.

=head2 context

    my $context = $object->context;
    $object->context(3);

=head2 xtype

    my $xtype = $object->xtype;
    $object->xtype(3);

=head2 actor

    my $actor = $object->actor;
    $object->actor(3);

=head2 object

    my $object = $object->object;
    $object->object(3);

=head2 signature

    my $signature = $object->signature;
    $object->signature(3);

=head2 published

    my $published = $object->published;
    $object->published(3);

=head1 SEE ALSO

L<MightyNetwork::DB>, L<Mojo::Base>

=cut

1;
