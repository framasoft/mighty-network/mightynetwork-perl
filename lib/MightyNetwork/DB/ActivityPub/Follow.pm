# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::ActivityPub::Follow;
use Mojo::Base 'MightyNetwork::DB', -signatures;
use Mojo::Collection 'c';
use MightyNetwork::DB::ActivityPub::Follow;

has 'table' => 'ap_follows';
has 'actor';
has 'xtype';
has 'items';

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::DB::ActivityPub::Follow

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 DESCRIPTION

Model for ActivityPub follows.

=head1 ATTRIBUTES

L<MightyNetwork::DB::ActivityPub::Follow> inherits attributes from L<MightyNetwork::DB> and adds the following attributes.

=head2 actor

    my $actor = $object->actor;
    $object->actor(3);

=head2 xtype

    my $xtype = $object->xtype;
    $object->xtype(3);

=head2 items

    my $items = $object->items;
    $object->items(3);

=head1 METHODS

L<MightyNetwork::DB::ActivityPub::Follow> inherits methods from L<MightyNetwork::DB> and adds the following methods.

=head2 create_followers_from_actor($actor)

Create a L<MightyNetwork::DB::ActivityPub::Follow> of type C<Followers> from a L<MightyNetwork::DB::ActivityPub::Actor>.

    my $actor     = MightyNetwork::DB::ActivityPub::Actor->new(…);
    my $followers = MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                          ->create_followers_from_actor($actor);

=cut

sub create_followers_from_actor($c, $actor) {
    return $c->create({
        actor => $actor->url,
        xtype => 'Followers',
        items => []
    });
}

=head2 create_following_from_actor($actor)

Create a L<MightyNetwork::DB::ActivityPub::Follow> of type C<Following> from a L<MightyNetwork::DB::ActivityPub::Actor>.

    my $actor     = MightyNetwork::DB::ActivityPub::Actor->new(…);
    my $following = MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                          ->create_following_from_actor($actor);

=cut

sub create_following_from_actor($c, $actor) {
    return $c->create({
        actor => $actor->url,
        xtype => 'Following',
        items => []
    });
}

=head2 get_collection_from_actor($actor)

Returns a L<Mojo::Collection> of L<MightyNetwork::DB::ActivityPub::Follow> objects belonging to the given L<MightyNetwork::DB::ActivityPub::Actor>.

    my $actor      = MightyNetwork::DB::ActivityPub::Actor->new(…);
    my $collection = MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                           ->get_collection_from_actor($actor);

=cut

sub get_collection_from_actor($c, $actor) {
    my @collection;
    $c->app->dbi->db->select($c->table, undef, {actor => $actor->url})
      ->hashes->each(sub($e, $num) {
          push @collection, MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                                  ->map_fields_to_attr($e);
      });
    return c(@collection);
}

=head1 MODIFIED METHODS

L<MightyNetwork::DB::ActivityPub::Follow> also modifies the following methods.

=head2 to_json(@to_delete)

It adds the C<items> and C<xtype> attributes to the list of attributes to delete from the JSON representation of the object.

    my $json = $object->to_json('foo', 'bar');

See L<MightyNetwork::DB#to_json-to_delete> for the original method.

=cut

sub to_json($c, @to_delete) {
    push @to_delete, 'items', 'xtype';
    return $c->SUPER::to_json(@to_delete);
}

=head1 SEE ALSO

L<MightyNetwork::DB>, L<MightyNetwork::DB::ActivityPub::Actor>, L<Mojo::Base>, L<Mojo::Collection>

=cut

1;
