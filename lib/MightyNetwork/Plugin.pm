# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Plugin;
use Mojo::Base;

=pod

=head1 NAME

MightyNetwork::Plugin

=head1 DESCRIPTION

Empty module, just here to provide this doc.

=head1 REFERENCE

This is the class hierarchy of L<MightyNetwork::Plugin>.

=over 2

=item L<MightyNetwork::Plugin>

=over 2

=item L<MightyNetwork::Plugin::Helpers>

=back

=back

=head1 SEE ALSO

L<MightyNetwork>, L<Mojo::Base>

=cut


1;
