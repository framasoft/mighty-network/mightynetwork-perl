# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::Auth;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use MightyNetwork::DB::User;
use MightyNetwork::DB::ActivityPub::Actor;
use MightyNetwork::DB::ActivityPub::Follow;
use Crypt::PBKDF2;

=pod

=head1 NAME

MightyNetwork::Controller::Auth

=head1 DESCRIPTION

=head1 METHODS

L<MightyNetwork::Controller::Auth> inherits methods from L<Mojolicious::Controller> and adds the following methods.

=cut

sub register($c) {
    my $v = $c->validation;
    $v->required('username');
    $v->required('password');
    $v->required('password2')->equal_to('password');
    $v->csrf_protect;

    return $c->render(template => 'register') if $v->has_error;

    my $username   = $v->param('username');
    my $password   = $v->param('password');

    my $pbkdf2 = Crypt::PBKDF2->new(
        hash_class   => 'HMACSHA2',
        hash_args    => {
            sha_size => 512,
        },
        iterations   => 10000,
        salt_len     => 10
    );
    my $hash = {
        username => $username,
        password => $pbkdf2->generate($password)
    };
    my $user = MightyNetwork::DB::User->new(app => $c->app)
                                      ->create($hash);

    if ($user) {
        my $actor     = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app)
                                                             ->create_from_user($user, $c->req->url);
        my $followers = MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                              ->create_followers_from_actor($actor);
        my $following = MightyNetwork::DB::ActivityPub::Follow->new(app => $c->app)
                                                              ->create_following_from_actor($actor);
        $c->render(
            template => 'login',
            messages => 'You are now registered. Please login for more fun.'
        );
    } else {
        $c->render(
            template => 'index',
            messages => 'This username is already taken.'
        );
    }
}

sub login($c) {
    return $c->redirect_to('index') if ($c->is_user_authenticated);

    my $validation = $c->validation;
    $validation->required('username');
    $validation->required('password');
    $validation->csrf_protect;
    return $c->render(template => 'login') if $validation->has_error;

    if ($c->authenticate($c->param('username'), $c->param('password'))) {
        $c->flash(messages => 'You are successfully logged in.');
        return $c->redirect_to('index');
    } else {
        return $c->render(
            template => 'login',
            messages => 'Wrong credentials. Try again.'
        );
    }
}

sub get_out($c) {
    return $c->redirect_to('index') unless ($c->is_user_authenticated);

    $c->logout();
    $c->flash(messages => 'You have been logged out.');
    return $c->redirect_to('index');
}

=head1 SEE ALSO

L<MightyNetwork::Controller>, L<Mojolicious::Controller>, L<Mojo::Base>

=cut

1;
