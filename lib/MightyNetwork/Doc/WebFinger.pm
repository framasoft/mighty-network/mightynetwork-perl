# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::WebFinger;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::WebFinger

=head1 DESCRIPTION

L<WebFinger|https://webfinger.net/> is a protocol specified by the L<IETF|https://ietf.org/> for discovery of information about people and things identified by a URI.

WebFinger is not specified in ActivityPub protocol so it’s optional but it’s used by many federated softwares as a de facto standard.

It’s used to fetch L<actor|MightyNetwork::Doc::Glossary#Actor> informations.

=head2 Request

In order to get information about the user C<foo> on the L<instance|MightyNetwork::Doc::Glossary#Instance> C<bar.org>, you need to send a WebFinger request to its instance.

    curl https://bar.org/.well-known/webfinger?resource=foo@bar.org

=head2 Response

You will get such a response:

    {
        "subject": "acct:foo@bar.org",
        "aliases": [
            "https://bar.org/users/foo"
        ],
        "links": [
            {
                "rel": "self",
                "type": "application/activity+json",
                "href": "https://bar.org/users/foo"
            }
        ]
    }

=head2 Use the response in your ActivityPub software

You will use the C<href> content of the link which C<rel> is C<self> to fetch actor’s informations.

    curl -H "Accept: application/activity+json" https://bar.org/users/foo

=head1 SEE ALSO

L<MightyNetwork::Doc>

=cut

1;
