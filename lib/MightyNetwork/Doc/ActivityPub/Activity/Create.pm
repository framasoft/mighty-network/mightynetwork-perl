# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Activity::Create;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Activity::Create

=head1 DESCRIPTION

L<Activity|MightyNetwork::Doc::ActivityPub::Activity> indicating that the L<actor|MightyNetwork::Doc::ActivityPub::Actor> has created the object.

See L<https://www.w3.org/TR/activitystreams-vocabulary/#dfn-create> for details.

=head1 ACTOR

Activity’s C<actor> refer to the L<actor|MightyNetwork::Doc::ActivityPub::Actor> who makes the action.

=head1 OBJECT

Activity’s C<object> refer to the L<object|MightyNetwork::Doc::ActivityPub::Object> that is the subject of the action.

See L<https://www.w3.org/TR/activitystreams-vocabulary/#h-object-types> for available objects.

=head1 SUMMARY

Example: L<I<https://example.org/users/narf>|MightyNetwork::Doc::ActivityPub::Actor> L<I<Create>|MightyNetwork::Doc::ActivityPub::Activity::Create> a L<I<Note>|MightyNetwork::Doc::ActivityPub::Note>.

=head1 JSON-LD REPRESENTATION

For our example L<above|MightyNetwork::Doc::ActivityPub::Activity::Create#SUMMARY>, the activity would be like this.

    {
        "@context" : "https://www.w3.org/ns/activitystreams",
        "id" : "https://example.org/users/narf/note/5b44c2974a67a67eec472d82/activity",
        "type" : "Create",
        "actor" : "https://example.org/users/narf",
        "object": {
            "type": "Note",
            "id" : "https://example.org/users/narf/note/5b44c2974a67a67eec472d82",
            "to" : [ "https://www.w3.org/ns/activitystreams#Public" ],
            "cc" : [ "https://example.org/users/narf/followers" ],
            "attributedTo":"https://example.org/users/narf",
            "content": "Hello world !",
            "published":"2018-07-10T14:28:39.889Z",
        },
        "published" : "2018-07-10T14:28:39.889Z"
    }

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::ActivityPub::Actor>, L<MightyNetwork::Doc::ActivityPub::Activity>, L<object|MightyNetwork::Doc::ActivityPub::Object>, L<MightyNetwork::Doc::ActivityPub::Note>, L<https://www.w3.org/TR/activitystreams-vocabulary/#h-activity-types>, L<https://www.w3.org/TR/activitystreams-vocabulary/#h-object-types>

=cut

1;
