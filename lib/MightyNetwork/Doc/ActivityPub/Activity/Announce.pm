# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Activity::Announce;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Activity::Announce

=head1 DESCRIPTION

L<Activity|MightyNetwork::Doc::ActivityPub::Activity> indicating that the L<actor|MightyNetwork::Doc::ActivityPub::Actor> is calling the target's attention the object.

It’s used in Mastodon to I<boost> (share, retweet if you prefer) a toot.
For this case, as L<JSON-LD signatures|MightyNetwork::Doc::JSON-LD-signatures>’ usage is decreasing due to a lack of libraries for all languages, to ensure the veracity of the announced object, you should fetch it from its server of origin, not trust the object contained in the C<Announce>.

See L<https://www.w3.org/TR/activitystreams-vocabulary/#dfn-announce> for details.

=head1 ACTOR

Activity’s C<actor> refer to the L<actor|MightyNetwork::Doc::ActivityPub::Actor> who makes the action.

=head1 OBJECT

Activity’s C<object> refer to the L<object|MightyNetwork::Doc::ActivityPub::Object> that is the subject of the action.

See L<https://www.w3.org/TR/activitystreams-vocabulary/#h-object-types> for available objects.

=head1 SUMMARY


Example: L<I<https://example.org/users/narf>|MightyNetwork::Doc::ActivityPub::Actor> L<I<Announce>|MightyNetwork::Doc::ActivityPub::Activity::Announce> she had arrived at I<Work> (which is a I<Place> object).

=head1 JSON-LD REPRESENTATION

For our example L<above|MightyNetwork::Doc::ActivityPub::Activity::Announce#SUMMARY>, the activity would be like this.

    {
      "@context": "https://www.w3.org/ns/activitystreams",
      "summary": "Narf announced that she had arrived at work",
      "type": "Announce",
      "actor": {
        "type": "Person",
        "id": "https://example.org/users/narf",
        "name": "Narf"
      },
      "object": {
        "type": "Arrive",
        "actor": "https://example.org/users/narf",
        "location": {
          "type": "Place",
          "name": "Work"
        }
      }
    }

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::ActivityPub::Actor>, L<MightyNetwork::Doc::ActivityPub::Activity>, L<object|MightyNetwork::Doc::ActivityPub::Object>, L<MightyNetwork::Doc::ActivityPub::Note>, L<https://www.w3.org/TR/activitystreams-vocabulary/#h-activity-types>, L<https://www.w3.org/TR/activitystreams-vocabulary/#h-object-types>

=cut

1;
