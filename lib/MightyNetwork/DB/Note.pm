# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::Note;
use Mojo::Base 'MightyNetwork::DB', -signatures;
use Mojo::Collection;

has 'table' => 'notes';
has 'id';
has 'actor';
has 'xtype';
has 'content';
has 'xto';
has 'cc';
has 'attributed_to';
has 'published';
has 'actor_object';

=pod

=head1 NAME

MightyNetwork::DB::Note

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 DESCRIPTION

Model for notes.

=head1 ATTRIBUTES

L<MightyNetwork::DB::Note> inherits attributes from L<MightyNetwork::DB> and adds the following attributes.

=head2 id

    my $id = $object->id;
    $object->id(3);

=head2 actor

    my $actor = $object->actor;
    $object->actor(3);

=head2 xtype

    my $xtype = $object->xtype;
    $object->xtype(3);

=head2 content

    my $content = $object->content;
    $object->content(3);

=head2 xto

    my $xto = $object->xto;
    $object->xto(3);

=head2 cc

    my $cc = $object->cc;
    $object->cc(3);

=head2 attributed_to

    my $attributed_to = $object->attributed_to;
    $object->attributed_to(3);

=head2 published

    my $published = $object->published;
    $object->published(3);

=head2 actor_object

    my $actor_object = $object->actor_object;
    $object->actor_object(3);

=head1 METHODS

L<MightyNetwork::DB::Note> inherits methods from L<MightyNetwork::DB> and adds the following methods.

=head2 find_actor_notes($actor)

Returns a L<Mojo::Collection> of notes belonging to an actor.

The argument C<$actor> must be a L<MightyNetwork::DB::ActivityPub::Actor> object.

    my $actor = MightyNetwork::DB::ActivityPub::Actor->new(app => $c->app);
    $c->find_actor_notes($actor);

=cut

sub find_actor_notes($c, $actor) {
    my $notes = Mojo::Collection->new();
    $c->app->dbi->db->select(
        [$c->table, ['ap_actors', url => 'attributed_to']],
        undef,
        { attributed_to => $actor->url }, {-asc => 'published'}
    )->hashes->each(sub ($e, $num) {
        push @{$notes}, MightyNetwork::DB::Note->new(app => $c->app)
                                               ->map_fields_to_attr($e)
                                               ->record(1);
    });

    return $notes;
}

=pod

=head1 MODIFIED METHODS

L<MightyNetwork::DB::Note> also modifies the following methods.

=head2 to_json(@to_delete)

It adds the C<actor_object> attribute to the list of attributes to delete from the JSON representation of the object.

    my $json = $object->to_json('foo', 'bar');

See L<MightyNetwork::DB#to_json-to_delete> for the original method.

=cut

sub to_json($c, @to_delete) {
    push @to_delete, 'actor_object';
    return $c->SUPER::to_json(@to_delete);
}

=head1 SEE ALSO

L<MightyNetwork::DB>, L<MightyNetwork::DB::ActivityPub::Actor>, L<Mojo::Collection>, L<Mojo::Base>

=cut

1;
