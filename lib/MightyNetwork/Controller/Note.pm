# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Controller::Note;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use MightyNetwork::DB::ActivityPub::Note;
use MightyNetwork::DB::ActivityPub::Activity;

=pod

=head1 NAME

MightyNetwork::Controller::Note

=head1 DESCRIPTION

=head1 METHODS

L<MightyNetwork::Controller::Note> inherits methods from L<Mojolicious::Controller> and adds the following methods.

=head2 new_note()

=cut

sub new_note($c) {
    my $v = $c->validation;
    $v->required('content');
    $v->csrf_protect;

    return $c->render(template => 'index') if $v->has_error;

    my $user      = $c->current_user;
    my $actor     = $c->get_actor;
    my $followers = $actor->get_followers;

    my $hash = {
        id      => sprintf('%s/note/%s', $actor->id, $c->generate_uuid),
        xtype   => 'Note',
        content => $v->param('content'),
        cc      => [$actor->followers],
        to      => ['https://www.w3.org/ns/activitystreams#Public'],
        attributed_to => $actor->url,
        actor_object  => $actor->to_json,
        actor         => $actor->url
    };
    my $new_note = MightyNetwork::DB::ActivityPub::Note->new(app => $c->app)
                                                       ->create($hash);

    $hash = {
        id        => sprintf('%s/activity', $new_note->id),
        context   => 'https://www.w3.org/ns/activitystreams',
        xtype     => 'Create',
        actor     => $new_note->actor,
        object    => $new_note->to_json,
        published => $new_note->published
    };
    my $new_activity = MightyNetwork::DB::ActivityPub::Activity->new(app => $c->app)
                                                               ->create($hash);
}

=head2 get_note()

=cut

sub get_note($c) {
}

=head1 SEE ALSO

L<MightyNetwork::Controller>, L<Mojolicious::Controller>, L<Mojo::Base>

=cut

1;
