# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::Doc::ActivityPub::Outbox;
use Mojo::Base;

=pod

=encoding UTF-8

=head1 NAME

MightyNetwork::Doc::ActivityPub::Outbox

=head1 DESCRIPTION

URL, linked to an L<actor|MightyNetwork::Doc::ActivityPub::Actor> returning a L<collection|MightyNetwork::Doc::ActivityPub::Collection> of the L<activities|MightyNetwork::Doc::ActivityPub::Activity> of an actor.
Useful when you start following an actor.

=head1 SEE ALSO

L<MightyNetwork::Doc>, L<MightyNetwork::Doc::ActivityPub>, L<MightyNetwork::Doc::ActivityPub::Actor>, L<MightyNetwork::Doc::ActivityPub::Activity>, L<MightyNetwork::Doc::ActivityPub::Collection>

=cut

1;
