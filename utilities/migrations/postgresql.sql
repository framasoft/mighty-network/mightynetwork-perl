-- 1 up
CREATE TABLE IF NOT EXISTS notifications (
    xtype     text,
    actor     text,
    object    text,
    published timestamp DEFAULT now()
);
CREATE TABLE IF NOT EXISTS users (
    id         uuid UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    username   text UNIQUE NOT NULL,
    password   text NOT NULL,
    created_at timestamp NOT NULL DEFAULT now()
);
CREATE TABLE IF NOT EXISTS ap_activities (
    context   text,
    xtype     text,
    id        text,
    actor     text,
    object    text,
    signature jsonb,
    published timestamp
);
CREATE TABLE IF NOT EXISTS ap_actors (
    user_id     uuid,
    id          uuid UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
    url         text UNIQUE NOT NULL,
    username    text NOT NULL,
    host        text NOT NULL,
    inbox       text UNIQUE NOT NULL,
    outbox      text UNIQUE NOT NULL,
    following   text UNIQUE NOT NULL,
    followers   text UNIQUE NOT NULL,
    public_key  text UNIQUE,
    private_key text,
    created_at  timestamp DEFAULT now(),
    FOREIGN KEY (user_id) REFERENCES users(id)
);
CREATE TABLE IF NOT EXISTS notes (
    id            text PRIMARY KEY,
    actor         text,
    xtype         text   NOT NULL,
    content       text   NOT NULL,
    xto           text[] NOT NULL,
    cc            text[] NOT NULL,
    attributed_to text   NOT NULL,
    published     timestamp DEFAULT now(),
    actor_object  text,
    FOREIGN KEY (attributed_to) REFERENCES ap_actors(url)
);
CREATE TABLE IF NOT EXISTS ap_collections (
    context       text,
    id            text,
    xtype         text,
    total_items   integer,
    ordered_items jsonb
);
CREATE TABLE IF NOT EXISTS ap_follows (
    actor text,
    xtype text,
    items text[]
);
-- 1 down
DROP TABLE IF EXISTS ap_follows;
DROP TABLE IF EXISTS ap_collections;
DROP TABLE IF EXISTS notes;
DROP TABLE IF EXISTS ap_actors;
DROP TABLE IF EXISTS ap_activitys;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS notifications;
