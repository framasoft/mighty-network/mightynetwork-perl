# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork::DB::ActivityPub::Collection;
use Mojo::Base 'MightyNetwork::DB', -signatures;

has 'table' => 'ap_collections';
has 'id';
has 'context';
has 'xtype';
has 'total_items';
has 'ordered_items';

=pod

=head1 NAME

MightyNetwork::DB::ActivityPub::Collection

=head1 DESCRIPTION

Model for ActivityPub collections.

B<Disclaimer>: the objects that are mentionned here are objects as in L<Object-oriented programming|https://en.wikipedia.org/wiki/Object-oriented_programming>, not as L<ActivityPub objects|MightyNetwork::Doc::Glossary#Federated-non-federated-objects>.

=head1 ATTRIBUTES

L<MightyNetwork::DB::ActivityPub::Collection> inherits attributes from L<MightyNetwork::DB> and adds the following attributes.

=head2 id

    my $id = $object->id;
    $object->id(3);

=head2 context

    my $context = $object->context;
    $object->context(3);

=head2 xtype

    my $xtype = $object->xtype;
    $object->xtype(3);

=head2 total_items

    my $total_items = $object->total_items;
    $object->total_items(3);

=head2 ordered_items

    my $ordered_items = $object->ordered_items;
    $object->ordered_items(3);

=head1 SEE ALSO

L<MightyNetwork::DB>, L<Mojo::Base>

=cut

1;
