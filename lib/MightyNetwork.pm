# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package MightyNetwork;
use Mojo::Base 'Mojolicious', -signatures, -async_await;
use MightyNetwork::DefaultConfig qw($default_config);
use MightyNetwork::DB::User;
use Crypt::PBKDF2;

=pod

=head1 NAME

MightyNetwork - Perl implementation of L<mightynetwork|https://framagit.org/narf/mightynetwork>, a simple L<ActivityPub|https://activitypub.rocks/> proof of concept.

=head1 DESCRIPTION

ActivityPub is complex. In order to make it easier to understand for everyone, having a sort of L<Rosetta stone|https://en.wikipedia.org/wiki/Rosetta_Stone> is needed.
Mightynetwork can be this Rosetta stone!

=head1 DOCUMENTATION

L<Narf|https://framagit.org/narf>, the developper of the original mightynetwork wrote a document explaining ActivityPub and its implementation in mightynetwork: L<https://frama.link/ActivityPubTech> (french).

See L<MightyNetwork::Doc> for documentation about ActivityPub.

=head1 SOFTWARE

L<MightyNetwork> is written in L<Perl|https://perl.org>, uses L<Mojolicious> as framework and L<PostgreSQL|https://postgresql.org> as database.

You can get the source code on L<https://framagit.org/framasoft/mighty-network/mightynetwork-perl>.

=cut

# This method will run once at server start
sub startup($self) {
    # Load configuration from hash returned by "my_app.conf"
    my $config = $self->plugin('Config' => {
        default => $default_config
    });

    $self->plugin('MightyNetwork::Plugin::Helpers');

    $self->plugin('Authentication' =>
        {
            autoload_user => 1,
            session_key   => 'MightyNetwork',
            stash_key     => '__authentication__',
            load_user     => sub($c, $uid) {
                return undef unless defined $uid;
                return MightyNetwork::DB::User->new(app => $c->app)->get_user_by_id($uid);
            },
            validate_user => sub($c, $username, $password, $extradata) {
                my $user = MightyNetwork::DB::User->new(app => $c->app)->get_user_by_username($username);
                if (defined($user->id)) {
                    my $hash   = $user->password;
                    my $pbkdf2 = Crypt::PBKDF2->new;
                    if ($pbkdf2->validate($hash, $password)) {
                        return $user->id;
                    }
                }
                return undef;
            }
        }
    );

    $self->plugin('DebugDumperHelper');
    $self->plugin('PODViewer' => { default_module => 'MightyNetwork', layout => 'podviewer', allow_modules => ['^MightyNetwork'] });
    $self->plugin('Status') if $self->mode eq 'development';

    # Default layout
    $self->defaults(layout => 'default');

    # Allow some redirects for the user agent
    $self->ua->max_redirects(3);

=head1 ROUTES

    /perldoc                    *     perldoc             => leads to this documentation
      +/:module                 *     module              => shows the documentation of a module
    /                           GET   "index"             => index page, shows a form to post a note if logged in
    /                           POST  "new_note"          => post a note, needs to be authenticated
    /register                   GET   "register"          => registering page
    /register                   POST  "register"          => register request
    /login                      GET   "login"             => login page
    /login                      POST  "login"             => login request
    /logout                     GET   "logout"            => logout request, redirects to /
    /note.json                  GET   "get_note"
    /notifications              GET   "notifications"
    /users                      *     users
      +/                        GET   "users"
      +/                        POST
      +/actors                  GET   actors
      +/:username               GET   username
      +/account/:id             GET   "account"
      +/:username/note/:id      GET   usernamenoteid
      +/users/:username/outbox  GET   "u_outbox"
      +/:username/inbox         GET   "u_inbox"
      +/:username               GET   "get_user"
    /.well-known/webfinger      GET   wellknownwebfinger
    /inbox                      GET   "inbox"

=cut

    # Router
    my $r = $self->routes;

    $r->get('/')
      ->to('Misc#index')
      ->name('index');

    $r->post('/')
      ->over(authenticated => 1)
      ->to('Note#new')
      ->name('new_note');

    $r->get('/register' => sub($c) {
        return $c->render(
            template => 'register',
        );
    })->name('register');

    $r->post('/register')
      ->to('Auth#register')
      ->name('register');

    $r->get('/login' => sub($c) {
        return $c->redirect_to('index') if ($c->is_user_authenticated);
        return $c->render(
            template => 'login',
        );
    })->name('login');

    $r->post('/login')
      ->to('Auth#login')
      ->name('login');

    $r->get('/logout')
      ->to('Auth#get_out')
      ->name('logout');

    $r->get('/note.json')
      ->to('Note#get_note')
      ->name('get_note');

    $r->get('/notifications')
      ->to('Notification#notifications')
      ->name('notifications');

    my $users = $r->under('/users');
    $users->get('/')
          ->to('Users#get_users')
          ->name('users');

    $users->post('/')
          ->to('Users#search_users');

    $users->get('/actors')
          ->to('Users#get_actors');

    $users->get('/:username')
          ->to('Users#username');

    $users->get('/account/:id')
          ->to('Users#account')
          ->name('account');

    $users->get('/:username/note/:id')
          ->to('Users#get_note');

    $users->get('/users/:username/outbox')
      ->to('Users#outbox')
      ->name('u_outbox');

    $users->get('/:username/inbox')
      ->to('Users#inbox')
      ->name('u_inbox');

    $users->get('/:username')
      ->to('Users#get_user')
      ->name('get_user');

    $r->get('/.well-known/webfinger')
      ->to('Users#webfinger');

    $r->get('/inbox')
      ->to('Users#Inbox')
      ->name('inbox');
}

=head1 REFERENCE

This is the class hierarchy of L<MightyNetwork>.

=over 2

=item L<MightyNetwork>

=over 2

=item L<MightyNetwork::Controller>

=over 2

=item L<MightyNetwork::Controller::ActivityPub>

=over 2

=item L<MightyNetwork::Controller::ActivityPub::Follow>

=item L<MightyNetwork::Controller::ActivityPub::Inbox>

=item L<MightyNetwork::Controller::ActivityPub::Outbox>

=back

=item L<MightyNetwork::Controller::Auth>

=item L<MightyNetwork::Controller::Index>

=item L<MightyNetwork::Controller::Misc>

=item L<MightyNetwork::Controller::Note>

=item L<MightyNetwork::Controller::Notification>

=item L<MightyNetwork::Controller::Users>

=item L<MightyNetwork::Controller::WebFinger>

=back

=item L<MightyNetwork::DB>

=over 2

=item L<MightyNetwork::DB::ActivityPub>

=over 2

=item L<MightyNetwork::DB::ActivityPub::Activity>

=item L<MightyNetwork::DB::ActivityPub::Actor>

=item L<MightyNetwork::DB::ActivityPub::Collection>

=item L<MightyNetwork::DB::ActivityPub::Follow>

=back

=item L<MightyNetwork::DB::Note>

=item L<MightyNetwork::DB::Notification>

=item L<MightyNetwork::DB::User>

=back

=item L<MightyNetwork::DefaultConfig>

=item L<MightyNetwork::Doc>

=over 2

=item L<MightyNetwork::Doc::ActivityPub>

=over 2

=item L<MightyNetwork::Doc::ActivityPub::Activity>

=over 2

=item L<MightyNetwork::Doc::ActivityPub::Activity::Announce>

=item L<MightyNetwork::Doc::ActivityPub::Activity::Create>

=item L<MightyNetwork::Doc::ActivityPub::Activity::Delete>

=back

=item L<MightyNetwork::Doc::ActivityPub::Actor>

=item L<MightyNetwork::Doc::ActivityPub::Collection>

=item L<MightyNetwork::Doc::ActivityPub::Inbox>

=item L<MightyNetwork::Doc::ActivityPub::SharedInbox>

=item L<MightyNetwork::Doc::ActivityPub::Note>

=item L<MightyNetwork::Doc::ActivityPub::Outbox>

=back

=item L<MightyNetwork::Doc::Glossary>

=item L<MightyNetwork::Doc::JSON-LD>

=item L<MightyNetwork::Doc::JSON-LD-signatures>

=item L<MightyNetwork::Doc::HTTP-signatures>

=item L<MightyNetwork::Doc::WebFinger>

=back

=item L<MightyNetwork::Plugin>

=over 2

=item L<MightyNetwork::Plugin::Helpers>

=back

=back

=back

=head1 SEE ALSO

L<Mojolicious>

=cut

1;
